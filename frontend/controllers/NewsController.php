<?php

namespace frontend\controllers;
use app\models\News;

class NewsController extends \yii\web\Controller
{
    public function actionIndex()
    {
        $model=new News();
        return $this->render('index',['model'=>$model]);
    }
    
    public function actionFull() {
        /* Здесь не будем передавать всю модель, а только массив данных для вывода. 
         * Ну чисто как другой способ, и шаблон будет "Чище" */
        $model=new News();
        $request=\Yii::$app->request->get();
        $news_id=$request['news_id'];
        $full=$model->Full($news_id);
        return $this->render('full',['full'=>$full]);       
    }
    
    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

}
