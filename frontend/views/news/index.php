<?php

/* @var $this yii\web\View */

$this->title = 'My Yii Application';
use yii\widgets\Menu;
use common\Helpers\TextcutHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\LinkPager;
use yii\data\Pagination;
?>
<div class="site-index">
    <div class="body-content">

        <div class="row">
     
            <div class="col-md-12">
<?php
            $news=$model->newsList();
            foreach ($news['items'] as $item) {
?>
                <div class="row">
                    <div class="col-md-12">
                        <h3><?php echo $item['title']; ?></h3>
                        <div class="test-date"><span class="glyphicon glyphicon-calendar"></span> <?php echo $item['date']; ?></div>
                        <h4><?php echo $item['themename']; ?></h4>
                        <p><?php echo TextcutHelper::cut($item['text']); ?></p>
                        <div class="morelink" style="text-align:right;"><?php echo Html::a('Читать далее', Url::to(['news/full','news_id'=>$item['news_id']])); ?></div>
                    </div>
                </div>
<?php
            }
?>
<?php
            $pagination = new Pagination(['totalCount'=>$news["total_count"],'pageSize'=>$news["itemsperpage"]]);
            echo LinkPager::widget([
                'pagination'=>$pagination,
            ]);
?>
            </div>
        </div>

    </div>
</div>
