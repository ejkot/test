<?php
/* Вьюха полного текста новости */
use yii\helpers\Html;
use yii\helpers\Url;
?>
<div class="row">
    <div class="col-md-12">
        <h1><?php echo $full["title"]; ?></h1>
        <div class="test-date"><span class="glyphicon glyphicon-calendar"></span> <?php echo $full['date']; ?></div>
        <h4><?php echo $full['themename']; ?></h4>
        <p><?php echo $full['text']; ?></p>
        <a href="/">Все новости</a>
    </div>
</div>

