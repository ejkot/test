<?php

namespace app\models;

use Yii;

use yii\db\Connection;
use yii\helpers\Url;
use yii\web\HttpException;
/**
 * Класс модели новостей
 * 
 */
class News extends \yii\base\Model
{
    /**
     * @var integer Количество новостей на одну страницу
     */
    public $itemsPerPage=5;
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'news';
    }
    /**
     * Имя таблицы тем
     * @return string
     */
    public static function tableThemeName() {
        return 'themes';
    }
    
    /**
     * Создадим массив Меню-календаря с пунктами в формате 
     * виджета yii\widgets\Menu;
     * по данным из БД
     * @return array Ассоциативный массив элементов меню
     */
    public static function makeYearMenuArray(){
        $connection=Yii::$app->getDb();
        /* Запрашиваем сгруппированный по месяцам список */
        $sql="SELECT YEAR(`date`) as year,MONTHNAME(`date`) as monthname,MONTH(`date`) as monthnum,COUNT(*) as cnt FROM ".self::tableName()." GROUP BY MONTH(`date`);";
        $cmd=$connection->createCommand($sql);
        $qr=$cmd->queryAll();
        /* Создаем массив требуемого формата */
        if (is_array($qr) && !empty($qr)) {
            $result=[];
            foreach ($qr as $r) {
        /* Чтобы все группировалось по годам, и избежать дополнительного запроса к БД, используем в качестве ключа год.
         * Создаем запись о годе если ее нет*/
                if (!array_key_exists($r["year"],$result)) {
                    $result[$r["year"]]=[
                        'label'=>$r["year"],
                        'url'=>Url::to(['/news/index','year'=>$r["year"]]),                   
                    ];
                }
        /* Докидываем в годы информацию о месяцах */
                $result[$r["year"]]['items'][]=[
                    'label'=>$r["monthname"]." (".$r["cnt"].")",
                    'url'=>Url::to(['/news/index','year'=>$r["year"],'monthnum'=>$r['monthnum']]),
                    'cnt'=>$r["cnt"],
                ];
            }
            
        }
     return $result;   
    }
   /**
    * Создадим массив меню тем,с пунктами в формате 
    * виджета yii\widgets\Menu;
    * по данным из БД
    * @return array Ассоциативный массив элементов меню
    */
    public static function makeThemesMenuArray(){
        $connection=Yii::$app->getDb();
        $sql="SELECT `news`.`theme_id`,`themes`.`theme_title`,COUNT(*) as cnt FROM `".self::tableName()."` JOIN `".self::tableThemeName()."` ON (`".self::tableName()."`.`theme_id`=`".self::tableThemeName()."`.`theme_id`) GROUP BY `news`.`theme_id`";
        $cmd=$connection->createCommand($sql);
        $qr=$cmd->queryAll();
        /* Создаем массив требуемого формата */
        if (is_array($qr) && !empty($qr)) {
            $result=[];
            foreach ($qr as $r) {
                $result[]=[
                    'label'=>$r["theme_title"]." (".$r["cnt"].")",
                    'url'=>Url::to(['/news/index','theme_id'=>$r['theme_id']]),
                ];
            }
        }
        return $result;
    }
    
/**
 * Получаем список новостей, согласно параметрам из Url
 * @return array Массив необходимых данных для шаблона
 * ['total_count'=>{total_count},'itemsperpage'=>{itemsPerPage},'items'=>{news_items}]
 */    
    public function newsList() {
        $params=  Yii::$app->request->get();
        /* Если страница не передана, берем нулевую */
        if (!isset($params['page']) || (int)$params['page']<1) $page=1; else $page=(int)$params['page'];
        /* Создадим sql - строку условий фильтрации */
        $where=""; 
        if (isset($params['theme_id'])) $where.=" AND `".$this->tableName ()."`.`theme_id`=:theme_id";
        if (isset($params['year'])) $where.=" AND YEAR(`date`)=:year";
        if (isset($params['monthnum']) && isset($params['year'])) $where.=" AND MONTH(`date`)=:monthnum";
        /* Создадим ограничения для пагинатора */
        $limit="";
        $start_line=($page-1)*$this->itemsPerPage;
        $limit=" LIMIT $start_line,$this->itemsPerPage";

        $connection=Yii::$app->getDb(); 
        /* Спросим у базы сколько вообще записей с такими условиями фильра в ней есть */
        $sql="SELECT COUNT(*) as cnt FROM `".$this->tableName()."` WHERE 1 ";
        $sql.=$where;
        $cmd=$this->prepareQuery($connection,$sql,$params);
        $qr=$cmd->queryOne();
        $total_count=(int)$qr["cnt"];
        /* Получим список всех записей подпадающих под фильтр */
        $sql="SELECT `news_id`,`date`,`news`.`title`,`text`,`themes`.`theme_title` as themename"
                . " FROM `".$this->tableName()."`"
                . " JOIN `".$this->tableThemeName()."`"
                . " ON (`".$this->tableName()."`.`theme_id`=`".$this->tableThemeName()."`.`theme_id`) "
                . "WHERE 1";       
        $sql.=$where;
        $sql.=$limit;       
        $cmd=$this->prepareQuery($connection,$sql,$params);
        $qr=$cmd->queryAll();
        if (!$qr) throw new HttpException(404,"Новости не найдены");
        /* Результирующий массив, содержащий в себе всю необходимую информацию */
        $result=['total_count'=>$total_count,'itemsperpage'=>$this->itemsPerPage,'items'=>$qr];
        return $result;
    }
    
    /**
     * Метод возвращает данные по одной новости
     * @param integer $news_id - идентификатор нужной новости
     * @return array массив данных новости
     * @throws HttpException вброс 404 если новость не найдена
     */
 
    public function Full($news_id) {
        $sql="SELECT `news_id`,`date`,`news`.`title`,`text`,`themes`.`theme_title` as themename"
                . " FROM `".$this->tableName()."`"
                . " JOIN `".$this->tableThemeName()."`"
                . " ON (`".$this->tableName()."`.`theme_id`=`".$this->tableThemeName()."`.`theme_id`) "
                . "WHERE news_id=:news_id";
        $connection=Yii::$app->getDb();
        $cmd=$connection->createCommand($sql,['news_id'=>(int)$news_id]);
        $qr=$cmd->queryOne();
        if (!$qr) throw new HttpException(404,"Такая новость не найдена");
        return $qr;
    }
    
    /* Биндим параметры к запросу */
    private function prepareQuery($connection,$sql,$params) {
        $cmd=$connection->createCommand($sql);
        if (isset($params['theme_id'])) $cmd->bindValue(":theme_id",(int)$params['theme_id']);
        if (isset($params['year'])) $cmd->bindValue(":year",$params['year']);
        if (isset($params['monthnum']) && isset($params['year'])) $cmd->bindValue(":monthnum",$params['monthnum']);       
        return $cmd;
    }
    
    
    
}
