<?php
/**
 * Хелпер обрезающий строку до N символов
 * 
 * @author A.Pitolin
 */
namespace common\Helpers;
class TextcutHelper {
    /**
     * Обрезает строку
     * @param string $str Исходная строка для обрезки
     * @param type $len Длина обрезки, по умолчанию 256
     * @return string Обрезанная строка
     */
    public static function cut($str,$len=256) {
    /* З.ы. по уму надо бы обрезать хитрее, чтобы слова не резались. например
       поискать первый пробел влево от позиции $len, но такой задачи пока не стоит     */
        return substr($str,0,$len)." …";
    }
}
