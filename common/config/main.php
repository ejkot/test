<?php
return [
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'components' => [
        'db' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'mysql:host=localhost;dbname=test',
            'username' => 'test',
            'password' => 'test',
            'charset' => 'utf8',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'assetManager'=>[
            'forceCopy'=>true,
             'linkAssets' => true,
        ],
        'urlManager'=>[
            'class'=>'yii\web\UrlManager',
            'showScriptName'=>false,
            'enablePrettyUrl'=>true,
        ],
    ],
];
