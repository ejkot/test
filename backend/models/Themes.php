<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "themes".
 *
 * @property integer $theme_id
 * @property string $theme_title
 */
class Themes extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'themes';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['theme_title'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'theme_id' => 'Theme ID',
            'theme_title' => 'Theme Title',
        ];
    }
}
