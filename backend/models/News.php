<?php

namespace app\models;

use Yii;
use yii\data\ActiveDataProvider;

/**
 * This is the model class for table "news".
 *
 * @property integer $news_id
 * @property string $date
 * @property integer $theme_id
 * @property string $text
 * @property string $title
 */
class News extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'news';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['date'], 'safe'],
            [['theme_id'], 'integer'],
            [['text'], 'string'],
            [['title'], 'string', 'max' => 255],
            [['text','title','date','themename'],'required'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'news_id' => '#ID',
            'date' => 'Дата',
            'theme_id' => 'Тема',
            'text' => 'Текст новости',
            'title' => 'Заголовок',
            'themename'=>'Тема',
        ];
    }
    /**
     * Метод для GridView, возвращает dataProvider
     * @param array $params Параметры из командной строки
     * @return ActiveDataProvider
     */
    
    public function search($params) {
        $query=self::find();
        $dataProvider=new ActiveDataProvider([
            'query'=>$query,
            'sort'=>array(
                'defaultOrder'=>['date'=>SORT_DESC],
            ),
            'pagination' => [
                'pagesize' => 5,
            ],
        ]);
        return $dataProvider;
    }
    
    /**
     * Опишем связь с таблицей themes
     */
    public function getThemes() {
        return $this->hasOne(Themes::className(), ['theme_id'=>'theme_id']);
    }
    
    /**
     * Геттер заголовков тем согласно связи
     * @return string Заголовок темы
     */
    public function getThemeName() {
        return $this->themes ? $this->themes->theme_title : "";
    }
    
    public function setThemeName($val) {
        $this->ThemeName=$val;
    }
    
}
