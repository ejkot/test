<?php
/* @var $this yii\web\View */
use yii\grid\GridView;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\Pjax;
use yii\bootstrap\Modal;
?>
<?php
/* Заставим модальное окно обновляться при каждом открытии */
$this->registerJs("$('body').on('hidden.bs.modal', '.modal', function () {
    $(this).removeData('bs.modal');
});");
?>
<h1>Редактирование новостей</h1>
<?php Pjax::begin(); ?>
<?php
    echo GridView::widget([
        'dataProvider'=>$model->search(Yii::$app->request->queryParams),
       /* 'filterModel'=>$model, */
        'tableOptions'=>['class'=>'ade_table table table-striped','id'=>$model->tableName()],
        'columns'=>[
            'news_id',
            'date:date',
            'ThemeName',
            'title',
            'text',
            ['class'=>'yii\grid\ActionColumn',
             'template'=>'{update} {delete}',
             'header'=>'Действия',
             'buttons'=>[
                 'update'=>function($url,$model) {
                    return Html::a('<span class="glyphicon glyphicon-pencil"></span>',$url,['class'=>'','data-toggle'=>'modal','data-target'=>'#editnews']);
                 }
             ],
            ]
        ],
    ]);

?>
        <?php echo Html::a('Добавить новость',['news/create'],['class'=>'btn btn-primary','data-toggle'=>'modal','data-target'=>'#editnews']);?>
<?php Pjax::end(); ?>

    <?php
       echo Modal::widget([
       'options'=>[
           'id'=>'editnews',
       ],
       'size'=>'modal-lg',
       'clientOptions'=>false,
    ]);

?>
