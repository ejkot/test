<?php
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\models\Themes;
use yii\jui\DatePicker;
?>
<div class="row">
    <div class="col-md-12">
        <div class="modal-form">
<?php $form = ActiveForm::begin([
    'id'=>'news-form',
    'action'=>Url::to(['/news/saveform','news_id'=>$model->news_id]),
    'options'=> [
        'class' => 'form-vertical',
        ],
]); ?>
<?php HTML::errorSummary($model); ?>
<div class="row">
    <div class="col-md-6">
       <?php echo $form->field($model,'date')->widget(DatePicker::className(),['name'=>'date',
           'value'=>date("Y-m-d"),
           'language'=>'ru-RU',
           'options'=>[
               'class'=>'form-control',
               'readonly'=>'readonly',
               ],
           'dateFormat'=>'yyyy-MM-dd',
           ]); ?>
    </div>
    <div class="col-md-6"><?php echo $form->field($model,'theme_id')->dropDownList(ArrayHelper::map(Themes::find()->All(), 'theme_id', 'theme_title'),['prompt'=>'Выберите тему']); ?></div>
</div>
    <?php echo $form->field($model,'title'); ?>
    <?php echo $form->field($model,'text')->textarea(['rows'=>6]); ?>
    <?php echo Html::submitButton('Сохранить', ['class' => 'btn btn-primary']); ?>
<?php 
     ActiveForm::end();
?>  
        </div>
    </div>
</div>
