<?php

namespace backend\controllers;

use app\models\News;
use yii\helpers\Url;
use yii\web\HttpException;
/**
 * Класс - контроллер редактирования новостей
 * 
 */
class NewsController extends \yii\web\Controller
{
/**
 * Вывод списка новостей
 * @return html Страница со списком
 */
    public function actionIndex()
    {
        $model=new News();
        return $this->render('index',['model'=>$model]);
    }
    
    public function actionCreate() {
        $model=new News();
        $model->ThemeName='';
        $model->date=date("Y-m-d");
        return $this->renderAjax('newsform',['model'=>$model]);
    }
    
    public function actionUpdate($id=NULL) {
        $news_id=$id;
        if ($news_id) {
            $model=News::findOne($news_id);
            return $this->renderAjax('newsform',['model'=>$model]);
        } else throw new HttpException(404 ,'Item not found');
    }
    
    public function actionDelete($id=NULL) {
        $news_id=$id;
        if ($news_id) {
           $model=News::findOne($news_id);
           $model->delete();
           $this->redirect (Url::to('/news/index'));
        } else throw new HttpException(404 ,'Item not found');
    }
    
    public function actionSaveform($news_id=NULL){
        if (!$news_id) {
            $model=new News();
        } else {
            $model=News::findOne($news_id);
        }
        if ($model->load(\Yii::$app->request->post())) {
                if ($model->save()) {
                    $this->redirect (Url::to('/news/index'));
                } else \Yii::$app->getSession()->setFlash('error', 'Save error');
        } else {

        };  
    $this->redirect (Url::to('/news/index'));
    }

}
