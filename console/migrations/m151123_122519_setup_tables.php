<?php

use yii\db\Schema;
use yii\db\Migration;

class m151123_122519_setup_tables extends Migration
{
    public function up()
    {

        $this->createTable('themes', [
            'theme_id'=>Schema::TYPE_PK,
            'theme_title'=>Schema::TYPE_STRING.'(255)',
        ]);
        $this->batchInsert('themes', ['theme_id','theme_title'], [
            [1, 'Наука'],
            [2, 'Спорт'],
            [3, 'Интернет'],
            [4, 'Авто'],
            [5, 'Глямур'],
            [6, 'Искусство'],
        ]);
        
        $this->createTable('news',[
            'news_id'=>Schema::TYPE_PK,
            'date'=>Schema::TYPE_DATE,
            'theme_id'=>Schema::TYPE_INTEGER,
            'text'=>Schema::TYPE_TEXT,
            'title'=>Schema::TYPE_STRING.'(255)',
        ]);

        $this->batchInsert('news',['date','theme_id','text','title'],[
            ['2014-05-02','1','Lorem ipsum maecenas eu magna porttitor rutrum massa orci in eros ornare tellus enim sapien proin, tellus. Fusce molestie ultricies nibh justo odio tempus vitae commodo pellentesque non orci leo ligula tempus, vitae porta mauris eros tellus. Risus maecenas, leo ipsum nec nibh amet nibh morbi bibendum. Ligula metus ipsum et maecenas vitae justo urna auctor ligula, sem vitae.','Lorem ipsum maecenas'],
            ['2014-05-03','2','Bibendum orci congue lorem vitae ultricies porta arcu, bibendum molestie eros tempus molestie cursus. A ornare nec, eros sodales vitae quam arcu sodales tempus molestie risus sagittis metus in nibh orci. Maecenas donec ipsum integer bibendum, diam mauris adipiscing maecenas justo bibendum tellus et sem sapien integer.','Sapien curabitur eu nulla massa'],
            ['2015-01-03','1','Ornare sodales orci vivamus urna tellus commodo sagittis commodo et orci eget sagittis rutrum diam et leo eu lectus quisque, cursus ornare nibh nam. Massa maecenas sodales malesuada odio rutrum morbi lorem et diam: porta commodo sem fusce eu sem elementum. Sodales eros - ut vitae at pharetra pellentesque porta donec, metus vulputate: quam magna adipiscing mattis tellus amet auctor sed at cursus integer. Mattis enim et nulla odio ut nibh ornare leo arcu et gravida fusce mauris - molestie massa: nam.','Ornare sodales orci vivamus'],
            ['2015-01-03','5','Odio fusce lectus morbi molestie morbi sed elementum integer gravida porta arcu sem, congue sodales - duis eros integer nam urna. Non diam eu massa mauris nec pellentesque lorem, arcu tellus donec morbi tellus morbi tellus nam amet maecenas metus sed lectus sagittis, nec leo. Odio proin porta curabitur adipiscing in ipsum rutrum magna massa pharetra mattis porttitor tellus orci urna tempus curabitur tempus pellentesque nam non at. In sed enim mattis sagittis leo, nam pellentesque ultricies eget.','Odio fusce lectus morbi'],
            ['2015-02-01','1','Lorem ipsum maecenas eu magna porttitor rutrum massa orci in eros ornare tellus enim sapien proin, tellus. Fusce molestie ultricies nibh justo odio tempus vitae commodo pellentesque non orci leo ligula tempus, vitae porta mauris eros tellus. Risus maecenas, leo ipsum nec nibh amet nibh morbi bibendum. Ligula metus ipsum et maecenas vitae justo urna auctor ligula, sem vitae.','Lorem ipsum maecenas'],
            
        ]);
    }

    public function down()
    {
        $this->dropTable('themes');
        $this->dropTable('news');

        return true;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
