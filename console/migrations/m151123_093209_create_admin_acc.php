<?php

use yii\db\Schema;
use yii\db\Migration;
use common\models\User;
/**
 * Создаем учетную запись админа автоматически
 * 
 */

class m151123_093209_create_admin_acc extends Migration
{
    public function up()
    {

        $user=new User;
        $user = new User();
            $user->username = 'admin';
            $user->email = 'admin@example.com';
            $user->setPassword('admin');
            $user->generateAuthKey();
            if ($user->save()) {
                return true;
            } else return false;
    }

    public function down()
    {
        $this->delete('user', ['username'=>'admin']);
        return true;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
